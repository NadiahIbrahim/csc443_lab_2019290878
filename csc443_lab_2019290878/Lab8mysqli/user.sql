-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 11, 2021 at 01:35 AM
-- Server version: 5.7.24
-- PHP Version: 7.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `registration`
--

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `first_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) NOT NULL,
  `pass_word` text NOT NULL,
  `registration_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `first_name`, `last_name`, `email`, `pass_word`, `registration_date`) VALUES
(1, 'John', 'Hacker', 'john@gmail.com', 'abc123', '2021-06-10 23:05:12'),
(2, 'Amy', 'Scammer', 'amy @gmail.com', '123456', '2021-06-10 23:10:18'),
(4, 'Sally', 'Ahmed', 'sally@gmail.com', '936410', '2021-06-10 23:18:18'),
(5, 'Fall', 'Porter', 'fall@gmail.com', 'qpl001', '2021-06-10 23:18:18'),
(6, 'Key', 'Cheter', 'key@gmail.com', 'k110e6', '2021-06-10 23:19:10'),
(7, 'Kally', 'Barnet', 'kally@gmail.com', 'a77b88', '2021-06-10 23:19:10'),
(8, 'Alex', 'Zender', 'alex@gmail.com', 'p00129', '2021-06-10 23:19:17'),
(9, 'Nasy', 'Harith', 'nasy@gmail.com', '123kkk', '2021-06-10 23:19:17'),
(10, 'Zack', 'Edison', 'zack@gmail.com', 'www123', '2021-06-10 23:19:34');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
